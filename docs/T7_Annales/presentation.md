---
author: à compléter
title: Introduction
---

## Présentation générale

!!! abstract "Présentation"

    Dans cette rubrique, vous trouverez l'intégralité des sujets écrits et pratiques posés au baccalauréat.
    Pour chacune des années, on donne également un tableau listant les différents thèmes abordés.

## Définition des épreuves

### Objectifs

L'épreuve terminale obligatoire de la spécialité NSI porte sur l'ensemble du programme de la classe de Terminale en vigueur. Les notions du programme de la classe de Première en vigueur non approfondies en classe de Terminale, doivent être connues et mobilisables. Elles ne peuvent cependant pas constituer un ressort essentiel du sujet.

### Nature de l'épreuve

L'épreuve terminale obligatoire de spécialité NSI est composée de deux parties : une partie écrite et une partie pratique, chacune notée sur 20 points. La note de la partie écrite a un coefficient de 0,75 et celle de la partie pratique a un coefficient de 0,25. La note globale de l’épreuve est donnée sur 20 points.

??? abstract "Modalités de la partie écrite"

    Durée : 3h30

    Le sujet comporte trois exercices indépendants les uns des autres, qui permettent d'évaluer les connaissances et compétences des candidats.

??? abstract "Modalités de la partie pratique"

    Durée : 1h

    La partie pratique consiste en la résolution de deux exercices sur ordinateur, chacun étant noté sur 10 points.

     - Le premier exercice consiste à programmer un algorithme figurant explicitement au programme, ne présentant pas de difficulté particulière, dont on fournit une spécification.
        
     - Le second exercice permet de valider, à partir d'un programme fourni au candidat, des compétences en programmation comme, par exemple, compléter un programme afin de répondre à une spécification donnée, compléter un programme pour le documenter, ou encore compléter un programme en ajoutant des assertions, etc.

Pour plus de détails, on renvoie au <a href="../a_telecharger/Déroulé_épreuves.pdf">document officiel</a>.



