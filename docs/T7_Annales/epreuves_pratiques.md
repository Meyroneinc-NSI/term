---
author: à compléter
title: Epreuves pratiques
---

# Epreuves pratiques

??? note "Année 2021"

	<div class="centre">
	<iframe 
	src="../a_telecharger/Annee_2021/EP_sujets_2021.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

??? note "Année 2022"

	<div class="centre">
	<iframe 
	src="../a_telecharger/Annee_2022/EP_sujets_2022.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

??? note "Année 2023"

	<div class="centre">
	<iframe 
	src="../a_telecharger/Annee_2023/EP_sujets_2023.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

??? note "Année 2024"

	<div class="centre">
	<iframe 
	src="../a_telecharger/Annee_2024/EP_sujets_2024.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>