# &#10145; Bac Blanc 2024


## Enoncés et corrigés

??? note "Bac Blanc 2024 : J1"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/Bac_Blanc_2024_J1.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

??? note "Bac Blanc 2024 : J2"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/Bac_Blanc_2024_J2.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>
