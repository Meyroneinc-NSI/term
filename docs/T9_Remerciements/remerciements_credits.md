---
author: à compléter
title: Remerciements et crédits
---

# Remerciements et crédits



## Remerciements

Ce site n'aurait jamais pu voir le jour sans les personnes à l'origine des différentes ressources ayant permis ces divers cours et TP :

- G.Lassus,  
- C.Poulmaire,   
- P.Remy,  
- F.Nativel  


## Crédits

L'intégralité des ressources présentes sur ce site (pdf, notebooks, ressources téléchargeables) sont sous licence <a href='https://creativecommons.org/licenses/by-sa/4.0/' target="_blank">CC BY-SA 4.0</a>.

