---
author: Meyroneinc-Condy
title: T4.2 - Routage
---

!!! progNSI "Programme Terminale"

    ![](data/BO_routage.png)

# Cours

??? note "Routage : Cours"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/Routage.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

# Exercices

??? note "Routage : Exercices"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/Routage_exercices.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

??? note "Routage : Exercices - correction"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/Routage_exercices_correction.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

# Devoir(s)

??? note "Routage : Devoir"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/DS_Routage.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

??? note "Routage : Devoir - correction"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/DS_Routage_correction.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>
