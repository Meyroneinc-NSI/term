---
author: Meyroneinc-Condy
title: T4.1 - Processus
---

!!! progNSI "Programme Terminale"

    ![](data/BO_processus.png)

# Cours

??? note "Processus : Cours"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/Processus.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

# Exercices

??? note "Processus : Exercices"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/Processus_exercices.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

??? note "Processus : Exercices - correction"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/Processus_Exercices_correction.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

# Devoir(s) 

??? note "Processus : Devoir"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/DS_Processus.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

??? note "Processus : Devoir - correction"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/DS_Processus_correction.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

??? note "Processus : Devoir rattrapage"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/DS_Processus_rattrapage.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

??? note "Processus : Devoir rattrapage - correction"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/DS_Processus_rattrapage_correction.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>