---
author: Meyroneinc-Condy
title: T3.2 - Piles et Files
---

!!! progNSI "Programme Terminale"

    ![](data/BO_PileFile1.png)

    ![](data/BO_PileFile2.png)


# Cours

!!! note "Piles et Files : Cours"
    Voir Capytale

# Exercices

??? note "Pile : Exercices"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/Piles.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

??? note "Pile : Exercices - correction"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/Piles_Correction.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

??? note "File : Exercices"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/Files.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

??? note "File : Exercices - correction"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/Files_Correction.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>


# Devoir(s) 

??? note "Pile et File : Devoir"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/DS_Files_Piles.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

??? note "Pile et File :  Devoir - correction"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/DS_Files_Piles_Correction.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>
