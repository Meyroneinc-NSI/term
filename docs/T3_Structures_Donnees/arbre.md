---
author: Meyroneinc-Condy
title: T3.3 - Les Arbres
---

!!! progNSI "Programme Terminale"

    ![](data/BO1.png)

    ![](data/BO2.png)


# Cours

!!! note "Arbres : Cours"
    Voir Capytale

# Exercices

??? note "Arbres : Exercices"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/Arbres_exercices.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

??? note "Arbres : Exercices - correction"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/Arbres_Exercices_correction.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

# Devoir(s) 

??? note "Arbres  : Devoir"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/DS_arbres.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

??? note "Arbres  : Devoir - correction"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/DS_arbres_Correction.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>
