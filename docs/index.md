


## Théme(s) en cours :

!!! info "En cours :"
    - Modéle relationnel  
    - Langage SQL

!!! info "Grand Oral"
    - [un article sur les mots de passes](https://www.numerama.com/cyberguerre/1734250-un-mot-de-passe-de-8-caracteres-est-devenu-trop-faible-a-cause-des-avancees-technologiques.html){:target="_blank"} 


## Info

!!! note "IMPORTANT - Informations sur les épreuves de Juin 2025 :"
    - La totalité du programme pourra être évaluée.  
    - Coefficients des deux épreuves : 
        - 15 points pour l'épreuve écrite,  
        - 5 points pour l'épreuve pratique.  
 
## Notions à l'épreuve écrite de Juin 2024 : 

### &#10145; Thème : Structure de données  

□ Structure de données abstraites( file) : interface et implémentation  
□ Vocabulaire de la programmation objet : classes, attributs, méthodes, objets   
□ Listes, piles, files : structures linéaires.   
□ Dictionnaires, index et clé   
□ Arbres : structures hiérarchiques. Arbres binaires : nœuds, racines, feuilles, sous-arbres gauches, sous-arbres droits  
□ Les graphes  

### &#10145; Thème : Base de données  

□ Modèle relationnel : relation, attribut, domaine, clef primaire, clef étrangère, schéma relationnel 
□ Base de données relationnelle et SGBD.  
□ Langage SQL : requêtes d'interrogation et de mise à jour d'une base de données  

### &#10145; Thème : Architectures matérielles, système d’exploitation et réseaux(ARSE)  

□ Gestion des processus et des ressources par un système d'exploitation  
□ Protocoles de routage  
□ Système à puce  
□ Sécurisation des communications  

### &#10145; Thème : Langage de programmation(LP)  
	
□ Récursivité  
□ Modularité  	
□ Mise au point des programmes. Gestion des bugs.
□ Programmation dynamique 

### &#10145; Thème : Algorithmique(A)

□ Algorithmes sur les arbres binaires et sur les arbres binaires de recherche.  
□ Méthode « diviser pour régner »  
□ Recherche textuelle - Algorithme de Boyer Moore   
  

## Comment calculer sa note au BAC 2025 et Répartition des notes :


![Répartion des notes au BAC 2024](divers/data/r-partition-de-la-note-finale-gt-octobre-2023.jpg){:target="_blank"} 