---
author: Meyroneinc-Condy
title: T5.1 - Diviser pour régner
---

!!! progNSI "Programme Terminale"

    ![](data/BO_diviser_regner.png)


!!! abstract "Cours"
    - Voir Capytale  
    
!!! question "Fiche Révision : Ce qu’il faut savoir"  
    - Le diviser pour régner (Divide and conquer en anglais) est une méthode algorithmique basée sur le principe suivant :  
    - On prend un problème (généralement complexe à résoudre), on divise ce problème en une multitude de petits problèmes, l'idée étant que les "petits problèmes" seront plus simples à résoudre que le problème original. Une fois les petits problèmes résolus, on recombine les "petits problèmes résolus" afin d'obtenir la solution du problème de départ. Le paradigme "diviser pour régner" repose donc sur 3 étapes :  
        - DIVISER : le problème d'origine est divisé en un certain nombre de sous-problèmes  
        - RÉGNER : on résout les sous-problèmes (les sous-problèmes sont plus faciles à résoudre que le problème d'origine)  
        - COMBINER : les solutions des sous-problèmes sont combinées afin d'obtenir la solution du problème d'origine.  
    
    - Exemple d’algorithme basé sur la méthode diviser pour régner : le tri-fusion (vous devez connaître le principe de cet algorithme).

    - L’algorithme de tri-fusion a une complexité en $O(nlog_2(n))$

!!! question "Ce qu’il faut savoir faire"
    - être capable d’implémenter l’algorithme de tri-fusion en Python


??? note "Diviser pour régner : Exercices"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/diviser_pour_regner_exercices.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

??? note "Diviser pour régner : Exercices - Correction"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/diviser_pour_regner_exercices_correction.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>