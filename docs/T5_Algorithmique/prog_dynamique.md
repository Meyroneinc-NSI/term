---
author: Meyroneinc-Condy
title: T5.3 - Programmation Dynamique
---


!!! abstract "TP Cours"
    - Voir Capytale  
    
!!! question "Fiche Révision : Ce qu’il faut savoir"  
    - la programmation dynamique est une méthode algorithmique utilisée pour résoudre les problèmes d'optimisation (comme les méthodes gloutonnes vues en classe de première).  

    - la programmation dynamique consiste à résoudre un problème en le décomposant en sous-problèmes, puis à résoudre les sous-problèmes, des plus petits aux plus grands en stockant les résultats intermédiaires.  

!!! question "Ce qu’il faut savoir faire"
    - vous devez être capable d'utiliser la programmation dynamique dans des cas simples (par exemple le problème du rendu de monnaie).

??? note "Programmation Dynamique"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/ProgrammationDynamique.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

