# &#10145; GEIPI 2024


## Enoncés et corrigés

??? note "GEIPI 2024"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/GEIPI_2024.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

??? note "GEIPI 2024 : Corrigé"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/corrige-NSI-2024.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>
