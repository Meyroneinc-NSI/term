# &#10145; GEIPI 2023


## Enoncés et corrigés

??? note "GEIPI 2023"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/GEIPI_2023.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

??? note "GEIPI 2023 : Corrigé"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/corrige-NSI-2023.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>
