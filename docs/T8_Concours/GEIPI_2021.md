# &#10145; GEIPI 2021


## Enoncés et corrigés

??? note "GEIPI 2021"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/GEIPI_2021.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

??? note "GEIPI 2021 : Corrigé"
    <div class="centre">
    <iframe 
    src="../../a_telecharger/corrige-NSI-2021.pdf"
    width="1000" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>
